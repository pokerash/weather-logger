package com.pokerash.weatherlogger.util;

import android.widget.ImageView;
import android.widget.TextView;

import com.pokerash.weatherlogger.model.db.weather_logger.WeatherLoggerEnt;

public interface OnItemClickListener {
    void onItemClick(WeatherLoggerEnt item, ImageView imageView, TextView tv);
}
