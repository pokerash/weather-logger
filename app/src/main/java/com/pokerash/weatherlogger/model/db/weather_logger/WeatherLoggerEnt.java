package com.pokerash.weatherlogger.model.db.weather_logger;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.pokerash.weatherlogger.model.api.WeatherLoggerData;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.util.Locale;

@Entity
public class WeatherLoggerEnt {
    @PrimaryKey(autoGenerate = true)
    private long id;
    private String date;
    @Embedded(prefix = "logger_")
    @TypeConverters(WeatherListConverter.class)
    private WeatherLoggerData mWeatherLoggerData;

    public WeatherLoggerEnt() {
    }

    @Ignore
    public WeatherLoggerEnt(String date, WeatherLoggerData weatherLoggerData) {
        this.date = date;
        mWeatherLoggerData = weatherLoggerData;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public WeatherLoggerData getWeatherLoggerData() {
        return mWeatherLoggerData;
    }

    public void setWeatherLoggerData(WeatherLoggerData weatherLoggerData) {
        mWeatherLoggerData = weatherLoggerData;
    }

    public String getDate() {
       return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
