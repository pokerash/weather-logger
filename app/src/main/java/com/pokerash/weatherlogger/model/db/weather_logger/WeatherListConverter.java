package com.pokerash.weatherlogger.model.db.weather_logger;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pokerash.weatherlogger.model.api.Weather;

import java.lang.reflect.Type;
import java.util.List;

public class WeatherListConverter {
    @TypeConverter
    public String fromWeatherList(List<Weather> weatherList) {
        if (weatherList == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Weather>>() {
        }.getType();
        return gson.toJson(weatherList, type);
    }

    @TypeConverter
    public List<Weather> toWeatherList(String weatherString) {
        if (weatherString == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Weather>>() {
        }.getType();
        return gson.fromJson(weatherString, type);
    }
}
