package com.pokerash.weatherlogger.model.db.weather_logger;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {WeatherLoggerEnt.class}, version = 2)
public abstract class WeatherLoggerDb extends RoomDatabase {
    private static final Object sLock = new Object();
    private static WeatherLoggerDb INSTANCE;

    public static WeatherLoggerDb getInstance(Context context) {
        synchronized (sLock) {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        WeatherLoggerDb.class, "WeatherLogger.db")
                        .allowMainThreadQueries()
                        .build();
            }
            return INSTANCE;
        }
    }

    public abstract WeatherLoggerDao weatherLoggerDao();
}
