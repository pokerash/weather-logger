package com.pokerash.weatherlogger.model.db.weather_logger;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.pokerash.weatherlogger.model.api.Weather;

import java.util.List;

@Dao
public interface WeatherLoggerDao {
    @Query("SELECT * FROM weatherloggerent")
    LiveData<List<WeatherLoggerEnt>> getAll();

    @Query("SELECT * FROM weatherloggerent WHERE logger_id = :loggerId")
    WeatherLoggerEnt getByLoggerId(long loggerId);

    @Query("SELECT * FROM weatherloggerent WHERE logger_name = :loggerName")
    LiveData<List<WeatherLoggerEnt>> getByLoggerName(String loggerName);

    @Insert
    void insert(WeatherLoggerEnt weatherLoggerEnt);

    @Update
    void update(WeatherLoggerEnt weatherLoggerEnt);

    @Delete
    void delete(WeatherLoggerEnt weatherLoggerEnt);
}