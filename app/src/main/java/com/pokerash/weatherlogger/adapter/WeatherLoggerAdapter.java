package com.pokerash.weatherlogger.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pokerash.weatherlogger.R;
import com.pokerash.weatherlogger.model.db.weather_logger.WeatherLoggerEnt;
import com.pokerash.weatherlogger.util.OnItemClickListener;
import com.pokerash.weatherlogger.view.WeatherLoggerViewModel;
import com.pokerash.weatherlogger.viewholder.WeatherLoggerViewHolder;

import java.util.ArrayList;
import java.util.List;

public class WeatherLoggerAdapter extends RecyclerView.Adapter<WeatherLoggerViewHolder> {
    private List<WeatherLoggerEnt> data;
    private Context context;
    private LayoutInflater layoutInflater;
    private OnItemClickListener listener;
    private WeatherLoggerViewModel mWeatherLoggerViewModel;

    public WeatherLoggerAdapter(Context context, OnItemClickListener listener, WeatherLoggerViewModel viewModel) {
        this.data = new ArrayList<>();
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listener = listener;
        this.mWeatherLoggerViewModel = viewModel;
    }

    @Override
    public WeatherLoggerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.item_weather_logger, parent, false);
        return new WeatherLoggerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherLoggerViewHolder holder, int position) {
        WeatherLoggerEnt ent = data.get(position);
        holder.bind(context, listener, ent, mWeatherLoggerViewModel);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<WeatherLoggerEnt> newData) {
        if (data != null) {
            PostDiffCallback postDiffCallback = new PostDiffCallback(data, newData);
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(postDiffCallback);

            data.clear();
            data.addAll(newData);
            diffResult.dispatchUpdatesTo(this);
        } else {
            // first initialization
            data = newData;
        }
    }

    public void setData(List<WeatherLoggerEnt> newData, String cityName) {
        if (data != null) {
            PostDiffCallback postDiffCallback = new PostDiffCallback(data, newData);
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(postDiffCallback);

            data.clear();
            for (int i = 0; i < newData.size(); i++) {
                if (cityName.equals(newData.get(i).getWeatherLoggerData().getName())) {
                    data.add(newData.get(i));
                }
            }
            diffResult.dispatchUpdatesTo(this);
        } else {
            // first initialization
            data = newData;
        }
    }

    class PostDiffCallback extends DiffUtil.Callback {

        private final List<WeatherLoggerEnt> oldPosts, newPosts;

        public PostDiffCallback(List<WeatherLoggerEnt> oldPosts, List<WeatherLoggerEnt> newPosts) {
            this.oldPosts = oldPosts;
            this.newPosts = newPosts;
        }

        @Override
        public int getOldListSize() {
            return oldPosts.size();
        }

        @Override
        public int getNewListSize() {
            return newPosts.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldPosts.get(oldItemPosition).getId() == newPosts.get(newItemPosition).getId();
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return oldPosts.get(oldItemPosition).equals(newPosts.get(newItemPosition));
        }
    }
}
