package com.pokerash.weatherlogger.viewholder;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pokerash.weatherlogger.R;
import com.pokerash.weatherlogger.model.db.weather_logger.WeatherLoggerEnt;
import com.pokerash.weatherlogger.util.OnItemClickListener;
import com.pokerash.weatherlogger.view.WeatherLoggerViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeatherLoggerViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = WeatherLoggerViewHolder.class.getSimpleName();
    @BindView(R.id.iv_item_menu)
    ImageView ivMenu;
    @BindView(R.id.tv_temperature_value)
    TextView tvTemperature;
    @BindView(R.id.tv_date_value)
    TextView tvDate;
    @BindView(R.id.iv_weather)
    ImageView ivWeather;

    public WeatherLoggerViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(Context context, OnItemClickListener listener, WeatherLoggerEnt data, WeatherLoggerViewModel viewModel) {
        tvTemperature.setText(String.format("%s C", data.getWeatherLoggerData().getMain().getTemp()));
        tvDate.setText(data.getDate());

        switch (data.getWeatherLoggerData().getWeather().get(0).getMain()) {
            case "Rain":
                ivWeather.setImageResource(R.drawable.ic_rain);
                break;
            case "Snow":
                ivWeather.setImageResource(R.drawable.ic_snow);
                break;
            case "Clouds":
                ivWeather.setImageResource(R.drawable.ic_cloud);
                break;
            default:
                ivWeather.setImageResource(R.drawable.ic_sun);
                break;
        }

        ViewCompat.setTransitionName(ivWeather, data.getId() + "_image");
        ViewCompat.setTransitionName(tvTemperature, data.getId() + "_temperature");

        itemView.setOnClickListener(v -> listener.onItemClick(data, ivWeather, tvTemperature));

        ivMenu.setOnClickListener(v -> popUpMenu(v, context, data, viewModel));
    }

    private void popUpMenu(final View v, Context context, final WeatherLoggerEnt data, WeatherLoggerViewModel mViewModel) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_custom_location, null);
        TextInputLayout til = view.findViewById(R.id.til_city_name);
        til.setHint("Enter new temperature value");
        TextInputEditText tiet = view.findViewById(R.id.et_city_name);

        //creating a popup menu
        PopupMenu popup = new PopupMenu(v.getContext(), v);
        //inflating menu from xml resource
        popup.inflate(R.menu.menu_view_holder_popup);

        //adding click listener
        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_edit:
                    AlertDialog.Builder builder = new AlertDialog.Builder(context)
                            .setView(view)
                            .setPositiveButton("OK", (dialog, which) -> {
                                if (!TextUtils.isEmpty(tiet.getText())) {
                                    data.getWeatherLoggerData().getMain().setTemp(Double.parseDouble(tiet.getText().toString()));
                                    mViewModel.edit(data);
                                    dialog.dismiss();
                                } else {
                                    Snackbar.make(v, "Please enter temperature value", Snackbar.LENGTH_LONG).show();
                                }
                            })
                            .setNegativeButton("Cancel", (dialog, which) -> {
                                dialog.dismiss();
                            });
                    builder.create()
                            .show();
                    break;
                case R.id.action_delete:
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context)
                            .setTitle(R.string.s_alert_dialog_title_warning)
                            .setMessage(R.string.s_alert_dialog_message_delete_warning)
                            .setPositiveButton("OK", (dialogInterface, i) -> {
                                mViewModel.delete(data);
                                dialogInterface.dismiss();
                            })
                            .setNegativeButton("Cancel", (dialogInterface, i) -> {
                                dialogInterface.dismiss();
                            });
                    alertDialog.setCancelable(false);
                    alertDialog.create().show();
                    break;
            }
            return false;
        });
        //displaying the popup
        popup.show();
    }
}
