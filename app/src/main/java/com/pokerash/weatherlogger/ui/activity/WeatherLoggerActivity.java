package com.pokerash.weatherlogger.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.pokerash.weatherlogger.R;
import com.pokerash.weatherlogger.ui.fragment.WeatherLoggerFragment;

public class WeatherLoggerActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_logger);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.logger_container, WeatherLoggerFragment.newInstance())
                .commit();
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }
}
