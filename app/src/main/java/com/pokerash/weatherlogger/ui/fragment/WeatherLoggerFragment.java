package com.pokerash.weatherlogger.ui.fragment;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.pokerash.weatherlogger.BuildConfig;
import com.pokerash.weatherlogger.R;
import com.pokerash.weatherlogger.adapter.WeatherLoggerAdapter;
import com.pokerash.weatherlogger.model.db.weather_logger.WeatherLoggerEnt;
import com.pokerash.weatherlogger.ui.activity.WeatherLoggerActivity;
import com.pokerash.weatherlogger.util.DetailsTransition;
import com.pokerash.weatherlogger.util.InternetConnection;
import com.pokerash.weatherlogger.util.OnItemClickListener;
import com.pokerash.weatherlogger.view.WeatherLoggerPresenter;
import com.pokerash.weatherlogger.view.WeatherLoggerView;
import com.pokerash.weatherlogger.view.WeatherLoggerViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeatherLoggerFragment extends Fragment implements WeatherLoggerView, OnItemClickListener {
    public static final String APP_ID = BuildConfig.APP_KEY;
    private static final String TAG = WeatherLoggerFragment.class.getSimpleName();
    @BindView(R.id.fab_add_new_city)
    FloatingActionButton mFloatingActionButton;
    @BindView(R.id.sp_cities)
    Spinner mSpinner;
    @BindView(R.id.toolbar_logger)
    Toolbar mToolbar;
    @BindView(R.id.rv_logger)
    RecyclerView mRecyclerView;
    @BindView(R.id.pb_logger)
    ProgressBar mProgressBar;
    private View mView;
    private WeatherLoggerActivity mActivity;
    private WeatherLoggerPresenter mPresenter;
    private WeatherLoggerAdapter mAdapter;
    private List<String> cities;
    private String cityName;
    private WeatherLoggerViewModel mViewModel;

    public WeatherLoggerFragment() {
    }

    public static WeatherLoggerFragment newInstance() {
        return new WeatherLoggerFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (WeatherLoggerActivity) getContext();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_weather_logger, container, false);
        ButterKnife.bind(this, mView);

        mActivity.setSupportActionBar(mToolbar);
        if (mActivity.getSupportActionBar() != null) {
            mActivity.getSupportActionBar().setTitle(R.string.app_name);
        }
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewModel = ViewModelProviders.of(mActivity).get(WeatherLoggerViewModel.class);
        mAdapter = new WeatherLoggerAdapter(mActivity, this, mViewModel);
        initPresenter();
        setCitiesAdapter();
        addNewCity();
    }

    private void addNewCity() {
        mFloatingActionButton.setOnClickListener(v -> {
            mPresenter.addCity(cities, mSpinner);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        loadWeatherLogsFromDb(cityName);
    }

    private void setCitiesAdapter() {
        cities = new ArrayList<>();
        cities.add("Astana");
        cities.add("Riga");
        cities.add("Toronto");
        cities.add("London");
        cities.add("New York");

        mSpinner.setSelection(0);

        cityName = cities.get(0);
        ArrayAdapter<String> mAdapter = new ArrayAdapter<>(mActivity, android.R.layout.simple_spinner_dropdown_item, cities);
        mSpinner.setAdapter(mAdapter);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityName = cities.get(position);
                loadWeatherLogsFromDb(cityName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadWeatherLogsFromDb(String cityName) {
        mPresenter.loadFromDb(cityName);
    }

    private void initPresenter() {
        mPresenter = new WeatherLoggerPresenter(mActivity, mView, this, mAdapter, mViewModel);
    }

    @Override
    public void setRV(RecyclerView.Adapter adapter) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void setVisibilityProgressBar(int visibility) {
        switch (visibility) {
            case View.GONE:
                mProgressBar.setVisibility(visibility);
                mRecyclerView.setVisibility(View.VISIBLE);
                new Handler().postDelayed(() -> mRecyclerView.scrollToPosition(0), 200);
                break;
            case View.VISIBLE:
                mProgressBar.setVisibility(visibility);
                mRecyclerView.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_save, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity)
                    .setTitle("Warning")
                    .setMessage(String.format("Are you sure you want to see weather for city %s", cityName))
                    .setPositiveButton("OK", (dialog, which) -> {
                        if (InternetConnection.checkConnection(mActivity)) {
                            mPresenter.loadData(cityName, APP_ID);
                            dialog.dismiss();
                        } else {
                            Snackbar.make(mView, R.string.string_internet_connection_warning, Snackbar.LENGTH_INDEFINITE).show();
                        }
                    })
                    .setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
            builder.create()
                    .show();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(WeatherLoggerEnt item, ImageView imageView, TextView tvTemperature) {
        Fragment fragmentDetails = WeatherLoggerDetailFragment.newInstance(item);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragmentDetails.setSharedElementEnterTransition(new DetailsTransition());
            fragmentDetails.setEnterTransition(new Fade());
            setExitTransition(new Fade());
            fragmentDetails.setSharedElementReturnTransition(new DetailsTransition());
        }

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .addSharedElement(imageView, getString(R.string.s_iv_transition))
                .addSharedElement(tvTemperature, getString(R.string.s_temperature_tr_name))
                .replace(R.id.logger_container, fragmentDetails)
                .addToBackStack(null)
                .commit();
    }
}
