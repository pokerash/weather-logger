package com.pokerash.weatherlogger.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pokerash.weatherlogger.R;
import com.pokerash.weatherlogger.model.db.weather_logger.WeatherLoggerEnt;
import com.pokerash.weatherlogger.ui.activity.WeatherLoggerActivity;

import org.joda.time.DateTime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeatherLoggerDetailFragment extends Fragment {
    private static final String TAG = WeatherLoggerDetailFragment.class.getSimpleName();
    private static final String WEATHER_TAG = "Weather";
    private static final String TEMPERATURE_TAG = "Temperature";
    private static final String CITY_NAME_TAG = "CityName";

    private static final String WIND_SPEED_TAG = "WindSpeed";
    private static final String VISION_TAG = "Vision";
    private static final String SUNRISE_TAG = "Sunrise";
    private static final String SUNSET_TAG = "Sunset";

    @BindView(R.id.detail_image_view)
    ImageView ivWeather;
    @BindView(R.id.toolbar_logger_detail)
    Toolbar mToolbar;
    @BindView(R.id.tv_temperature)
    TextView tvTemperature;
    @BindView(R.id.tv_wind_speed)
    TextView tvWindSpeed;
    @BindView(R.id.tv_vision)
    TextView tvVision;
    @BindView(R.id.tv_sunrise)
    TextView tvSunrise;
    @BindView(R.id.tv_sunset)
    TextView tvSunset;

    private String weather;
    private Double temperature;
    private Double windSpeed;
    private Integer vision;
    private Long sunrise;
    private Long sunset;

    private WeatherLoggerActivity mActivity;

    public static WeatherLoggerDetailFragment newInstance(WeatherLoggerEnt ent) {
        WeatherLoggerDetailFragment fragment = new WeatherLoggerDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(CITY_NAME_TAG, ent.getWeatherLoggerData().getName());
        bundle.putString(WEATHER_TAG, ent.getWeatherLoggerData().getWeather().get(0).getMain());
        bundle.putDouble(TEMPERATURE_TAG, ent.getWeatherLoggerData().getMain().getTemp());
        bundle.putDouble(WIND_SPEED_TAG, ent.getWeatherLoggerData().getWind().getSpeed());
        bundle.putInt(VISION_TAG, ent.getWeatherLoggerData().getVisibility());
        bundle.putLong(SUNRISE_TAG, ent.getWeatherLoggerData().getSys().getSunrise());
        bundle.putLong(SUNSET_TAG, ent.getWeatherLoggerData().getSys().getSunset());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (WeatherLoggerActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather_logger_detail, container, false);
        ButterKnife.bind(this, view);

        mActivity.setSupportActionBar(mToolbar);
        if (mActivity.getSupportActionBar() != null) {
            mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mActivity.getSupportActionBar().setTitle(getArguments().getString(CITY_NAME_TAG));
        }

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getArgumentsFromBundle();
        initViews();
    }

    private void initViews() {
        tvTemperature.setText(String.format("%s C", temperature));

        switch (weather) {
            case "Rain":
                ivWeather.setImageResource(R.drawable.ic_rain);
                break;
            case "Snow":
                ivWeather.setImageResource(R.drawable.ic_snow);
                break;
            case "Clouds":
                ivWeather.setImageResource(R.drawable.ic_cloud);
                break;
            default:
                ivWeather.setImageResource(R.drawable.ic_sun);
                break;
        }

        tvWindSpeed.setText(String.valueOf(windSpeed));
        tvVision.setText(String.format("%s meters", vision));
        tvSunrise.setText(convertLongIntoTime(sunrise));
        tvSunset.setText(convertLongIntoTime(sunset));
    }

    private String convertLongIntoTime(long timeStamp){
        final DateTime dt = new DateTime(timeStamp * 1000L);
        return dt.toString("HH:mm");
    }

    private void getArgumentsFromBundle() {
        weather = getArguments().getString(WEATHER_TAG);
        temperature = getArguments().getDouble(TEMPERATURE_TAG);
        windSpeed = getArguments().getDouble(WIND_SPEED_TAG);
        vision = getArguments().getInt(VISION_TAG);
        sunrise = getArguments().getLong(SUNRISE_TAG);
        sunset = getArguments().getLong(SUNSET_TAG);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            mActivity.onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
