package com.pokerash.weatherlogger.network.api;

import com.pokerash.weatherlogger.network.ApiConstant;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {
    private static final long HTTP_TIMEOUT = 300000;
    private static RetrofitService sServiceService;

    public static RetrofitService getRetrofitService() {
        if (sServiceService == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .readTimeout(HTTP_TIMEOUT, TimeUnit.MILLISECONDS)
                    .connectTimeout(HTTP_TIMEOUT, TimeUnit.MILLISECONDS)
                    .addInterceptor(interceptor)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiConstant.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();

            sServiceService = retrofit.create(RetrofitService.class);
            return sServiceService;
        } else {
            return sServiceService;
        }
    }
}
