package com.pokerash.weatherlogger.network.api;

import com.pokerash.weatherlogger.model.api.WeatherLoggerData;
import com.pokerash.weatherlogger.network.ApiConstant;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitService {
    @GET(ApiConstant.CURRENT_WEATHER_DATA)
    Call<WeatherLoggerData> saveWeatherLoggerIntoDB(@Query("q") String name, @Query("units") String units, @Query("APPID") String appId);
}
