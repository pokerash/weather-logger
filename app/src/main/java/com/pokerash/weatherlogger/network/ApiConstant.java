package com.pokerash.weatherlogger.network;

import com.pokerash.weatherlogger.BuildConfig;

public class ApiConstant {
    public static final String APP_KEY = BuildConfig.APP_KEY;
    public static final String BASE_URL = BuildConfig.BASE_URL;
    public static final String CURRENT_WEATHER_DATA = BuildConfig.CURRENT_WEATHER_DATA;
}
