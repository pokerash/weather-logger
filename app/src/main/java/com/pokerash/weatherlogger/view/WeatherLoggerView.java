package com.pokerash.weatherlogger.view;

import android.support.v7.widget.RecyclerView;

public interface WeatherLoggerView {
    void setRV(RecyclerView.Adapter adapter);

    void setVisibilityProgressBar(int visibility);
}
