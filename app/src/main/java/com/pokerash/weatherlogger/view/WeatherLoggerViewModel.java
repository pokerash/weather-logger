package com.pokerash.weatherlogger.view;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.pokerash.weatherlogger.model.db.weather_logger.WeatherLoggerDao;
import com.pokerash.weatherlogger.model.db.weather_logger.WeatherLoggerDb;
import com.pokerash.weatherlogger.model.db.weather_logger.WeatherLoggerEnt;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class WeatherLoggerViewModel extends AndroidViewModel {
    private WeatherLoggerDao mDao;
    private ExecutorService executorService;

    public WeatherLoggerViewModel(@NonNull Application application) {
        super(application);
        mDao = WeatherLoggerDb.getInstance(application).weatherLoggerDao();
        executorService = Executors.newSingleThreadExecutor();
    }

    LiveData<List<WeatherLoggerEnt>> getAllLogs() {
        return mDao.getAll();
    }

    WeatherLoggerEnt getAllByLoggerId(int loggerId) {
        return mDao.getByLoggerId(loggerId);
    }

    LiveData<List<WeatherLoggerEnt>> getWeatherByName(String loggerName) {
        return mDao.getByLoggerName(loggerName);
    }

    public void delete(WeatherLoggerEnt ent) {
        executorService.execute(() -> mDao.delete(ent));
    }

    public void edit(WeatherLoggerEnt ent) {
        executorService.execute(() -> mDao.update(ent));
    }

    void saveLog(WeatherLoggerEnt post) {
        executorService.execute(() -> mDao.insert(post));
    }
}
