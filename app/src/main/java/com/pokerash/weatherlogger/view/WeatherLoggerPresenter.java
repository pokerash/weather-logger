package com.pokerash.weatherlogger.view;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pokerash.weatherlogger.R;
import com.pokerash.weatherlogger.adapter.WeatherLoggerAdapter;
import com.pokerash.weatherlogger.model.api.WeatherLoggerData;
import com.pokerash.weatherlogger.model.db.weather_logger.WeatherLoggerEnt;
import com.pokerash.weatherlogger.network.api.ServiceGenerator;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class WeatherLoggerPresenter {
    private static final String TAG = WeatherLoggerPresenter.class.getSimpleName();
    private AppCompatActivity mActivity;
    private View mView;
    private WeatherLoggerAdapter mAdapter;
    private WeatherLoggerView mLoggerView;
    private WeatherLoggerViewModel mViewModel;

    public WeatherLoggerPresenter(AppCompatActivity activity, View view, WeatherLoggerView loggerView, WeatherLoggerAdapter adapter, WeatherLoggerViewModel viewModel) {
        mActivity = activity;
        mView = view;
        mLoggerView = loggerView;
        mAdapter = adapter;
        mViewModel = viewModel;
    }

    public void addCity(List<String> cities, Spinner spinner) {
        View dialogView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_custom_location, null);
        TextInputLayout til = dialogView.findViewById(R.id.til_city_name);
        TextInputEditText tiet = dialogView.findViewById(R.id.et_city_name);
        AlertDialog.Builder dialog = new AlertDialog.Builder(mActivity)
                .setTitle("City name")
                .setView(dialogView)
                .setPositiveButton("Save", (dialog1, which) -> {
                    if (!TextUtils.isEmpty(tiet.getText())) {
                        til.setError(null);
                        til.setErrorEnabled(false);
                        cities.add(tiet.getText().toString());
                        spinner.setSelection(cities.size() - 1);
                        loadFromDb(tiet.getText().toString());
                        dialog1.dismiss();
                    } else {
                        til.setErrorEnabled(true);
                        til.setError("Enter city name or cancel");
                        Snackbar.make(mView, "Enter city name", Snackbar.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton("Cancel", (dialog12, which) -> dialog12.dismiss());
        dialog.create().show();
    }

    public void loadData(String cityName, String appId) {
        mLoggerView.setVisibilityProgressBar(VISIBLE);
        Call<WeatherLoggerData> call = ServiceGenerator.getRetrofitService()
                .saveWeatherLoggerIntoDB(cityName, "metric", appId);

        call.enqueue(new Callback<WeatherLoggerData>() {
            @Override
            public void onResponse(@NonNull Call<WeatherLoggerData> call, @NonNull Response<WeatherLoggerData> response) {
                mLoggerView.setVisibilityProgressBar(GONE);
                if (response.isSuccessful()) {
                    WeatherLoggerData data = response.body();
                    if (data != null) {
                        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                        DateTime jodaDate = dtf.parseDateTime(DateTime.now().toString());
                        DateTimeFormatter dtfDate = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm");
                        WeatherLoggerEnt ent = new WeatherLoggerEnt(dtfDate.print(jodaDate), data);
                        mViewModel.saveLog(ent);
                        loadFromDb(cityName);
                    }
                } else {
                    try {
                        Map responseMap;
                        if (response.errorBody() != null) {
                            responseMap = new ObjectMapper().readValue(response.errorBody().string(), HashMap.class);
                            Snackbar.make(mView, String.format("%s", responseMap.get("message")), Snackbar.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        Snackbar.make(mView, e.getMessage(), Snackbar.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<WeatherLoggerData> call, @NonNull Throwable t) {
                mLoggerView.setVisibilityProgressBar(GONE);
                Snackbar.make(mView, R.string.s_onFailure_msg, Snackbar.LENGTH_INDEFINITE).show();
            }
        });
    }

    public void loadFromDb(String cityName) {
        mViewModel.getWeatherByName(cityName).observe(mActivity, posts -> mAdapter.setData(posts, cityName));
        mLoggerView.setRV(mAdapter);
    }
}
